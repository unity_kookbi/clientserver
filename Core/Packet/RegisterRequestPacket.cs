﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core
{
    /// <summary>
    /// 회원가입 요청 패킷을 위한 구조체
    /// </summary>
    public struct RegisterRequestPacket
    {
        public string id;
        public string pw;
        public string nickname;

        public RegisterRequestPacket(string id, string pw, string nickname)
        {
            this.id = id;
            this.pw = pw;
            this.nickname = nickname;
        }

        public override string ToString()
        {
            return $"id:{id} / pw:{pw} / nickname:{nickname}";
        }
    }
}
