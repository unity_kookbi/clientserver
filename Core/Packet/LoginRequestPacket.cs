﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core
{
    public struct LoginRequestPacket
    {
        public string id;
        public string pw;

        public LoginRequestPacket(string id, string pw)
        {
            this.id = id;
            this.pw = pw;
        }
    }
}
