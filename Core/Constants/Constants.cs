﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core
{
    public class Constants
    {
        // 아이피 
        public const string ADDRESS = "192.168.3.226";
        //public const string ADDRESS = "192.168.3.90";

        // 포트
        public const int SERVERPORT = 20000;
        // 소켓을 구분짓는 식별자
        // 한 기기 내에서 중복되지 않도록 해야 합니다.
        // 1024부터 49000 포트까지 사용을 권장합니다.

        // 데이터베이스 이름
        public const string DB_SCHEMANAME = "sampledb";

        // 데이터베이스 ID
        public const string DB_ID = "root";

        // 데이터베이스 PW
        public const string DB_PW = "root";


    }
}
