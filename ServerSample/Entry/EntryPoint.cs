﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using Core;
using MySql.Data.MySqlClient;

namespace ServerSample;

// 네트워크
// 서로 다른 위치에 있는 기기를 여러가지 통신 매체를 이용하여 연결시켜 놓은 것

// Socket
// 물리적으로 잘 연결되어있는 네트워크 상에서 데이터 송수신에 사용할 수 있는 소프트웨어적 장치

public class EntryPoint
{

    // 소켓 객체
    private Socket _ServerSocket;

    // 연결된 클라이언트 객체들을 나타냅니다.
    public List<ConnectedClient> clients = new();

    /// <summary>
    /// MySql 연결 객체
    /// </summary>
    private MySqlConnection _MySqlConnection;

    /// <summary>
    /// 요청된 쿼리문
    /// </summary>
    private Queue<RequestQueryCommand> _RequestQueryCommands = new();


    private static async Task Main() 
        => await new EntryPoint().WaitNewConnection();

    public EntryPoint() 
    {
        IPEndPoint endPoint = new(IPAddress.Parse(Core.Constants.ADDRESS), Core.Constants.SERVERPORT);

        _ServerSocket = new(
            AddressFamily.InterNetwork, 
            SocketType.Stream, 
            ProtocolType.Tcp);

        /*
         TCP : (연결지향형)
         - 데이터의 순서와 도착을 보장합니다. (신뢰성 보장)
         - 1 : 1 연결
         - 게임 / 채팅 프로그램에서 주로 사용됩니다.


         UDP : (비 연경지향형)
         - 데이터의 순서와 도착을 보장하지 않습니다. (신뢰성을 보장받지 못함)
         - TCP 보다 빠릅니다.
         - 1 : N 연결이 가능
         - 보통 라이브 스트리밍에서 사용됩니다.
        */

        
        _ServerSocket.Bind(endPoint);
        _ServerSocket.Listen(15);
    }

    /// <summary>
    /// 새로운 연결을 대기합니다.
    /// </summary>
    /// <returns></returns>
    private async Task WaitNewConnection()
    {
        using (_MySqlConnection = new(
            //$"Server = 127.0.0.1;" +
            $"Server = localhost;" +
            $"Database = {Core.Constants.DB_SCHEMANAME};" +
            $"Uid={Core.Constants.DB_ID};" +
            $"Pwd={Core.Constants.DB_PW}"))
        {
            _MySqlConnection.Open();

            // 쿼리 명령어 프로시저 실행
            Thread queryCmdProc = new(QueryCommandProcedure);
            queryCmdProc.Start();

            // 새로운 접속 요청을 받습니다.
            while(true)
            {
            // 연결 요청된 새로운 클라이언트의 연결 요청을 수락합니다.
            Socket connectedClientSocket = await _ServerSocket.AcceptAsync();

            // 클라이언트 연결 요청을 수락하기 위하여 쓰레드 풀에 등록합니다.
            ThreadPool.QueueUserWorkItem(OnClientConnected, connectedClientSocket);
            }
        }
    }

    /// <summary>
    /// 클라이언트 데이터를 수신합니다.
    /// </summary>
    /// <param name="sender"></param>
    private void OnClientConnected(object sender)
    {
        // 연결된 클라이언트 소켓 객체를 얻습니다.
        Socket clientSocket = sender as Socket;

        // 연결된 클라이언트 객체
        ConnectedClient newConnection = new ConnectedClient(clientSocket);

        // 연결 해제 이벤트 등록
        newConnection.onDisconnected += OnClientDisconnected;

        // 쿼리문 요청을 위한 함수 등록
        newConnection.requestQueryCmd += RequestQueryCmd;

        // 공지 함수 등록
        newConnection.onAnnounced += OnAnnounced;

        // 연결된 클라이언트 객체를 리스트에 보관합니다.
        clients.Add(newConnection);
    }

    /// <summary>
    /// 하나의 클라이언트가 연결 해제될 경우 호출되는 콜백
    /// </summary>
    /// <param name="disconnectedClient"></param>
    private async void OnClientDisconnected(ConnectedClient disconnectedClient) 
    {
        // 로그인 된 유저가 종료한 경우
        if(disconnectedClient.isLogin)
        {
            string disconnectClientNickname = disconnectedClient.nickname;

            SimpleResponsePacket packet = new(true, disconnectClientNickname);

            await OnAnnounced(disconnectedClient, PacketType.OnUserLogout, packet);
        }

        Console.WriteLine($"연결이 종료되었습니다.[{disconnectedClient.clientSocket.RemoteEndPoint}]");
        
        // 연결 해제
        disconnectedClient.clientSocket.Shutdown(SocketShutdown.Send);

        // 연결된 클라이언트 목록에서 제거합니다.
        clients.Remove(disconnectedClient);

    }

    /// <summary>
    /// 공지된 내용이 존재하는 경우 호출됩니다.
    /// </summary>
    /// <param name="client">클라이언트 객체가 전달됩니다.</param>
    /// <param name="packetType">패킷 타입이 전달됩니다.</param>
    /// <param name="context">내용이 전달됩니다.</param>
    /// <returns></returns>
    private async Task OnAnnounced(ConnectedClient client, PacketType packetType, object context)
    {
        foreach(ConnectedClient connectedClient in clients)
        {
            // 새로운 유저가 로그인한 경우
            if(packetType == PacketType.OnNewUserLogin)
            {
                // 로그인 되지 않은 유저에게는 알리지 않습니다.
                if(!connectedClient.isLogin) continue;

                // 로그인된 다른 클라이언트에게 보낼 내용을 얻습니다.
                LoginResponsePacket response = (LoginResponsePacket)context;

                // 새로 접속한 유저에게는 이전에 접속된 유저 정보들을 모두 보냅니다.
                if(connectedClient == client)
                {
                    List<string> otherUserNicknames = new();

                    foreach(ConnectedClient online in clients)
                    {
                        // 로그인되지 않은 유저는 제외합니다.
                        if (!online.isLogin) continue;

                        // 자신은 제외합니다.
                        else if (online == client) continue;

                        // 로그인된 다른 유저들의 닉네임을 모두 추가합니다.
                        otherUserNicknames.Add(online.nickname);
                    }

                    // 이전부터 로그인된 유저들의 닉네임을 설정합니다.
                    response.otherUserNicknames = otherUserNicknames;
                }


                // 로그인된 유저 데이터를 다른 클라이언트에게 보냅니다.
                await Packet.SendAsync(connectedClient.clientSocket, packetType, response);

            }

            // 새로운 채팅이 추가된 경우
            else if (packetType == PacketType.OnNewChattingAdded)
            {
                // 로그인 되지 않은 유저에게는 보내지 않습니다.
                if (!connectedClient.isLogin) continue;

                // 보낸 채팅을 클라이언트들에게 전달합니다.
                await Packet.SendAsync(connectedClient.clientSocket, packetType, (NewChattingContext)context);
            }

            // 유저가 로그아웃 된 경우
            else if(packetType == PacketType.OnUserLogout)
            {
                if (client == connectedClient) continue;

                await Packet.SendAsync(connectedClient.clientSocket,
                    packetType, (SimpleResponsePacket)context);
            }
        }
    }

    private async void QueryCommandProcedure()
    {
        while(true)
        {
            // 처리할 요청이 존재하는지 확인합니다.
            // 처리할 요청이 없다면 대기합니다
            if (_RequestQueryCommands.Count == 0) continue;

            // 요청을 얻습니다.
            RequestQueryCommand requestQuery = _RequestQueryCommands.Dequeue();

            // 요청된 명령을 생성합니다.
            MySqlCommand cmd = new(requestQuery.queryCommand, _MySqlConnection);

            if(requestQuery.isNonQuery)
            {
                cmd.ExecuteNonQuery();
            }
            else
            {
                MySqlDataReader table = cmd.ExecuteReader();
                await requestQuery.onExcuteReader(table);
                await table.CloseAsync();
            }
        }
    }

    private void RequestQueryCmd(
        string queryCommand,
        System.Func<MySqlDataReader, Task> onExecuteReader = null)
    {
        // 다른 쓰레드에서 _RequestQueryCommands 에 대한 동시 접근을 제한합니다.
        lock (_RequestQueryCommands)
        {
            _RequestQueryCommands.Enqueue(
                new RequestQueryCommand(queryCommand, onExecuteReader));
        }
    }

}
