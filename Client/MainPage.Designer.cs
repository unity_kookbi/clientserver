﻿namespace Client
{
    partial class MainPage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Button_Login = new System.Windows.Forms.Button();
            Button_Register = new System.Windows.Forms.Button();
            label1 = new System.Windows.Forms.Label();
            TextBox_ID = new System.Windows.Forms.TextBox();
            TextBox_PW = new System.Windows.Forms.TextBox();
            label2 = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();
            SuspendLayout();
            // 
            // Button_Login
            // 
            Button_Login.Location = new System.Drawing.Point(267, 148);
            Button_Login.Name = "Button_Login";
            Button_Login.Size = new System.Drawing.Size(75, 62);
            Button_Login.TabIndex = 0;
            Button_Login.Text = "로그인";
            Button_Login.UseVisualStyleBackColor = true;
            // 
            // Button_Register
            // 
            Button_Register.Location = new System.Drawing.Point(267, 216);
            Button_Register.Name = "Button_Register";
            Button_Register.Size = new System.Drawing.Size(75, 23);
            Button_Register.TabIndex = 1;
            Button_Register.Text = "회원가입";
            Button_Register.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new System.Drawing.Font("한컴 고딕", 21.7499962F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            label1.Location = new System.Drawing.Point(97, 9);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(179, 38);
            label1.TabIndex = 2;
            label1.Text = "채팅프로그램";
            // 
            // TextBox_ID
            // 
            TextBox_ID.Location = new System.Drawing.Point(57, 153);
            TextBox_ID.Name = "TextBox_ID";
            TextBox_ID.Size = new System.Drawing.Size(204, 23);
            TextBox_ID.TabIndex = 3;
            // 
            // TextBox_PW
            // 
            TextBox_PW.Location = new System.Drawing.Point(57, 187);
            TextBox_PW.Name = "TextBox_PW";
            TextBox_PW.Size = new System.Drawing.Size(204, 23);
            TextBox_PW.TabIndex = 4;
            TextBox_PW.UseSystemPasswordChar = true;
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            label2.Location = new System.Drawing.Point(32, 156);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(20, 15);
            label2.TabIndex = 5;
            label2.Text = "ID";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            label3.Location = new System.Drawing.Point(26, 190);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(26, 15);
            label3.TabIndex = 6;
            label3.Text = "PW";
            // 
            // MainPage
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            ClientSize = new System.Drawing.Size(360, 246);
            Controls.Add(label3);
            Controls.Add(label2);
            Controls.Add(TextBox_PW);
            Controls.Add(TextBox_ID);
            Controls.Add(label1);
            Controls.Add(Button_Register);
            Controls.Add(Button_Login);
            Name = "MainPage";
            Text = "MainPage";
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private System.Windows.Forms.Button Button_Login;
        private System.Windows.Forms.Button Button_Register;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TextBox_ID;
        private System.Windows.Forms.TextBox TextBox_PW;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
    }
}