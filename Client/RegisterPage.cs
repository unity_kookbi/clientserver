﻿using Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Client
{
    public partial class RegisterPage : Form
    {
        public RegisterPage()
        {
            InitializeComponent();

            // 회원가입 버튼 클릭 이벤트 바인딩
            Button_Register.Click += OnRegisterButtonClicked;
        }

        private async void OnRegisterButtonClicked(object sender, EventArgs e)
        {
            bool isEmpty = false;

            // 아이디 문자열을 얻습니다.
            string idText = TextBox_ID.Text;
            if (string.IsNullOrEmpty(idText)) isEmpty = true;


            // 비밀번호 문자열을 얻습니다.
            string pwText = TextBox_PW.Text;
            if(string.IsNullOrEmpty(pwText)) isEmpty = true;

            // 닉네임 문자열을 얻습니다.
            string nicknameText = TextBox_Nickname.Text;
            if(string.IsNullOrEmpty (nicknameText)) isEmpty = true;

            // 비운 칸이 존재한다면
            if(isEmpty)
            {
                MessageBox.Show("문자열을 비울 수 없습니다.", "회원가입 실패");
                return;
            }

            bool isFailed = (idText.Length < 4) ||  // ID 문자열이 4자리 미만인 경우
                (pwText.Length < 6) ||              // PW 문자열이 6자리 미만인 경우
                (nicknameText.Length < 2);          // Nickname 문자열이 2자리 미만인 경우 실패
            
            if(isFailed)
            {
                MessageBox.Show(
                    "아이지 길이는 4자리 이상, 비밀번호는 6자리 이상, 닉네임은 2자리 이상",
                    "회원가입 실패");
                return;
            }

            // 요청시킬 데이터를 완성합니다.
            RegisterRequestPacket registerRequestPacket = new(idText, pwText, nicknameText);

            // 데이터를 서버로 보냅니다.
            await Packet.SendAsync(ClientInstance.instance.connectedServerSocket,PacketType.RegisterRequest, registerRequestPacket);

            // 서버에 대한 응답을 받습니다.
            await Packet.ReceiveAsyncPacketType(ClientInstance.instance.connectedServerSocket);

            // 회원가입 결과를 얻습니다.
            SimpleResponsePacket response = await Packet.ReceiveAsyncHeaderAndData<SimpleResponsePacket>(ClientInstance.instance.connectedServerSocket);

            // 회원가입 성공
            if(response.result)
            {
                DialogResult dlgResult = MessageBox.Show("회원가입 성공!", "회원가입 결과");
                if(dlgResult == DialogResult.OK)
                {
                    Close();
                }
            }
            // 회원가입 실패
            else
            {
                MessageBox.Show("회원가입 실패.", "회원가입 결과");
            }
        }

    }
}
