using Client;
using System;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClientSample
{
    internal static class Program
    {
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        private static async Task Main()
        {
            // To customize application configuration such as set high DPI settings or default font,
            // see https://aka.ms/applicationconfiguration.
            ApplicationConfiguration.Initialize();

            // 클라이언트 객체 생성
            ClientInstance.InitializeClientInstance();

            await ClientInstance.instance.ConnectAsync();

            MainPage mainPage = new MainPage();
            Application.Run(mainPage);

            //await mainPage.ConnectAsync();

        }
    }
}