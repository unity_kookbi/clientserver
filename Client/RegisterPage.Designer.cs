﻿namespace Client
{
    partial class RegisterPage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            label1 = new System.Windows.Forms.Label();
            TextBox_ID = new System.Windows.Forms.TextBox();
            TextBox_PW = new System.Windows.Forms.TextBox();
            TextBox_Nickname = new System.Windows.Forms.TextBox();
            label2 = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();
            label4 = new System.Windows.Forms.Label();
            Button_Register = new System.Windows.Forms.Button();
            SuspendLayout();
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new System.Drawing.Font("한컴 고딕", 21.7499962F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            label1.Location = new System.Drawing.Point(30, 9);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(125, 38);
            label1.TabIndex = 0;
            label1.Text = "회원가입";
            // 
            // TextBox_ID
            // 
            TextBox_ID.Location = new System.Drawing.Point(65, 87);
            TextBox_ID.Name = "TextBox_ID";
            TextBox_ID.Size = new System.Drawing.Size(107, 23);
            TextBox_ID.TabIndex = 1;
            // 
            // TextBox_PW
            // 
            TextBox_PW.Location = new System.Drawing.Point(65, 116);
            TextBox_PW.Name = "TextBox_PW";
            TextBox_PW.Size = new System.Drawing.Size(107, 23);
            TextBox_PW.TabIndex = 2;
            TextBox_PW.UseSystemPasswordChar = true;
            // 
            // TextBox_Nickname
            // 
            TextBox_Nickname.Location = new System.Drawing.Point(65, 145);
            TextBox_Nickname.Name = "TextBox_Nickname";
            TextBox_Nickname.Size = new System.Drawing.Size(107, 23);
            TextBox_Nickname.TabIndex = 3;
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            label2.Location = new System.Drawing.Point(20, 90);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(20, 15);
            label2.TabIndex = 4;
            label2.Text = "ID";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            label3.Location = new System.Drawing.Point(20, 119);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(26, 15);
            label3.TabIndex = 5;
            label3.Text = "PW";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Font = new System.Drawing.Font("맑은 고딕", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            label4.Location = new System.Drawing.Point(16, 148);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(43, 15);
            label4.TabIndex = 6;
            label4.Text = "닉네임";
            // 
            // Button_Register
            // 
            Button_Register.Font = new System.Drawing.Font("한컴 고딕", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            Button_Register.Location = new System.Drawing.Point(20, 174);
            Button_Register.Name = "Button_Register";
            Button_Register.Size = new System.Drawing.Size(152, 42);
            Button_Register.TabIndex = 7;
            Button_Register.Text = "회원가입";
            Button_Register.UseVisualStyleBackColor = true;
            // 
            // RegisterPage
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            ClientSize = new System.Drawing.Size(182, 228);
            Controls.Add(Button_Register);
            Controls.Add(label4);
            Controls.Add(label3);
            Controls.Add(label2);
            Controls.Add(TextBox_Nickname);
            Controls.Add(TextBox_PW);
            Controls.Add(TextBox_ID);
            Controls.Add(label1);
            Name = "RegisterPage";
            Text = "RegisterPage";
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TextBox_ID;
        private System.Windows.Forms.TextBox TextBox_PW;
        private System.Windows.Forms.TextBox TextBox_Nickname;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button Button_Register;
    }
}