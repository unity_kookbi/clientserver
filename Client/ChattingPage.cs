﻿using Core;
using Microsoft.VisualBasic.Devices;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Client
{
    public partial class ChattingPage : Form
    {
        public ChattingPage()
        {
            InitializeComponent();

            // 보내기 버튼 클릭 이벤트
            Button_Send.Click += OnSendButtonClicked;

            // 서버에서 보낸 데이터를 받기 위하여 데이터 수신용 쓰레드 실행
            Thread receiveThread = new Thread(async() => await ReceiveChattingProcedure());
            receiveThread.Start();

            TextBox_Chatting.ScrollBars = ScrollBars.Vertical;
            TextBox_Send.KeyDown += OnSendKeyDown;
        }

        public async Task ReceiveChattingProcedure()
        {
            while(true)
            {
                // 패킷 타입을 얻습니다.
                PacketType packetType = await Packet.ReceiveAsyncPacketType(ClientInstance.instance.connectedServerSocket);

                // 새로운 유저 로그인 시
                if(packetType == PacketType.OnNewUserLogin)
                {
                    // 새로 접속된 유저 정보를 얻습니다.
                    LoginResponsePacket packet = 
                        await Packet.ReceiveAsyncHeaderAndData<LoginResponsePacket>(
                            ClientInstance.instance.connectedServerSocket);    

                    Console.WriteLine($"새로운 유저 로그인 nickname : {packet.nickname}");

                    // 자신의 닉네임을 추가합니다.
                    AddUserNickname(packet.nickname);

                    // 다른 유저 닉네임을 추가합니다.
                    if(packet.otherUserNicknames != null)
                    {
                        foreach(string nickname in packet.otherUserNicknames)
                            AddUserNickname(nickname);
                    }
                }

                // 새로운 채팅이 추가된 경우
                else if(packetType == PacketType.OnNewChattingAdded)
                {
                    // 새로 추가된 채팅 내용을 얻습니다.
                    NewChattingContext packet = await Packet.ReceiveAsyncHeaderAndData<NewChattingContext>(ClientInstance.instance.connectedServerSocket);

                    // 얻은 내용을 채팅창에 추가합니다.
                    AddNewChatting(packet.nickname, packet.context);
                }

                // 로그아웃된 유저가 존재하는 경우
                else if(packetType == PacketType.OnUserLogout)
                {
                    SimpleResponsePacket packet = await Packet.ReceiveAsyncHeaderAndData<SimpleResponsePacket>(
                        ClientInstance.instance.connectedServerSocket);

                    // 리스트에서 제거
                    RemoveUserNickname(packet.context);
                }
            }
        }

        /// <summary>
        /// 접속한 유저 닉네임을 추가합니다.
        /// </summary>
        private void AddUserNickname(string nickname)
        {
            // 다른 쓰레드로부터 호출되었을 경우 Invoke 를 사용해야 하는지에
            // 대한 여부를 반환합니다.
            // True 를 반환하는 경우 Invoke 를 사용해서 대리자를 넘겨야 하며
            // False 인 경우 직접 컨트롤에 접근할 수 있습니다.
            if (ListBox_Users.InvokeRequired)
                ListBox_Users.Invoke(() => ListBox_Users.Items.Add(nickname));

            else ListBox_Users.Items.Add(nickname);
        }

        /// <summary>
        /// 연결 종료한 유저의 닉네임을 제거합니다.
        /// </summary>
        /// <param name="nickname"></param>
        private void RemoveUserNickname(string nickname)
        {
            if (ListBox_Users.InvokeRequired)
                ListBox_Users.Invoke(() => ListBox_Users.Items.Remove(nickname));

            else ListBox_Users.Items.Remove(nickname);
        }

        /// <summary>
        /// 새로운 채팅 내용을 채팅창에 추가합니다.
        /// </summary>
        /// <param name="nickname"></param>
        /// <param name="context"></param>
        private void AddNewChatting(string nickname, string context)
        {
            if (TextBox_Chatting.InvokeRequired)
            {
                TextBox_Chatting.Invoke(() => TextBox_Chatting.Text += $"{nickname} : {context}\r\n");

                TextBox_Chatting.Invoke(() => TextBox_Chatting.Select(TextBox_Chatting.Text.Length,0));
                
                TextBox_Chatting.Invoke(() => TextBox_Chatting.ScrollToCaret());
            }

            else
            {
                TextBox_Chatting.Text += $"{nickname} : {context}\r\n";

                TextBox_Chatting.Select(TextBox_Chatting.Text.Length, 0);
                
                TextBox_Chatting.ScrollToCaret();
            }
        }

        /// <summary>
        /// 전송 버튼 클릭 시 호출되는 메서드
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void OnSendButtonClicked(object sender, EventArgs e)
        {
            // 입력한 내용을 얻습니다.
            string inputString = TextBox_Send.Text;

            // 입력된 내용이 존재하지 않는 경우 호출 중단
            if (string.IsNullOrEmpty(inputString)) return;

            // 입력한 내용 비우기
            TextBox_Send.Text = null;

            // 입력한 내용 요청
            await Packet.SendAsync<NewChattingContext>(
                ClientInstance.instance.connectedServerSocket,
                PacketType.OnNewChattingRequest, new(ClientInstance.instance.thisUserNickname, inputString));
        }

        private void OnSendKeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                OnSendButtonClicked(sender,null);
            }
        }
    }
}
